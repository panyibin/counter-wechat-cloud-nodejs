const path = require("path");
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const { init: initDB, Counter } = require("./db");
const expressWs = require("express-ws");

const logger = morgan("tiny");

const app = express();
expressWs(app);

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
app.use(logger);

// websocket 
app.ws('/ws', (ws, req) => {
  console.log("websocket connection established");

  ws.on('message', (msg) => {
    console.log(`received message:${msg}`);
    ws.send(`Echo:${msg}`);

    const question = "how to be rich";

    const api_key = "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"

    const url = "https://service-9bsr306o-1317139396.usw.apigw.tencentcs.com/v1/chat/completions"

    let headers = {
        "Content-Type": "application/json",
        'Accept': 'text/event-stream',
        "Authorization": "Bearer " + api_key,
    }

    let data = {
        "model": "gpt-3.5-turbo",
        "messages": [{ "role": "user", "content": question }],
        "temperature": 0.6,
        "stream": true
    }

    const options = {
        url: url,
        headers:headers,
        body: JSON.stringify(data)
    };

    let completeJsonStr = '';
    let buffer = '';

    console.log("post openai api");
    request.post(options)
        .on('response', (response) => {
            // 监听响应的 response 事件
            console.log(`statusCode: ${response.statusCode}`);
            console.log(`headers: ${JSON.stringify(response.headers)}`);
        })
        .on('data', (chunk) => {
            buffer += chunk.toString();

            // 以 "\n" 分隔行
            const lines = buffer.split('\n');
          
            // 遍历所有行，找到以 "data: " 开头的行，对其进行处理
            for (let i = 0; i < lines.length; i++) {
              const line = lines[i].trim();
              if (line.startsWith('data: ')) {
                try {
                  // 去掉 "data: " 后解析为 JSON
                  const jsonStr = line.substring('data: '.length);
                  const json = JSON.parse(jsonStr);
          
                  // 处理解析出来的 JSON
                //   console.log('json is:');
                
                //   console.log(json.choices[0].delta.content);
                let content = json.choices[0].delta.content;
                  process.stdout.write(json.choices[0].delta.content);
                  ws.send(content);
                } catch (err) {
                //   console.error(err);
                }
              }
            }
          
            // 保留缓存区最后一行数据，因为可能不完整
            buffer = lines[lines.length - 1];
        })
        .on('error', (error) => {
            console.error(`Error: ${error}`);
        });
  });

  ws.on('close', ()=>{
    console.log('WebSocket connection closed');
  })

});

//

// 首页
app.get("/", async (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

// 更新计数
app.post("/api/count", async (req, res) => {
  const { action } = req.body;
  if (action === "inc") {
    await Counter.create();
  } else if (action === "clear") {
    await Counter.destroy({
      truncate: true,
    });
  }
  res.send({
    code: 0,
    data: await Counter.count(),
  });
});

// 获取计数
app.get("/api/count", async (req, res) => {
  const result = await Counter.count();
  res.send({
    code: 0,
    data: result,
  });
});

// 小程序调用，获取微信 Open ID
app.get("/api/wx_openid", async (req, res) => {
  if (req.headers["x-wx-source"]) {
    res.send(req.headers["x-wx-openid"]);
  }
});

// 首页
app.get("/api/test", async (req, res) => {
  console.log("received /api/test");
  res.send({message:'hello google'});
});

const port = process.env.PORT || 80;

async function bootstrap() {
  await initDB();
  app.listen(port, () => {
    console.log("启动成功", port);
  });
}

bootstrap();
